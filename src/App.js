import React, { useState, useEffect } from 'react';
import axios from 'axios';
import CreateUserForm from './components/CreateUserForm'
import User from './components/User'
import { makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
const useStyles = makeStyles((theme) => ({
  root: {

    width: '100%',
    maxWidth: '36ch',
    backgroundColor: theme.palette.background.paper,
  },
  inline: {
    display: 'inline',
  },
  header: {
    backgroundColor: 'black',
    color: 'white',
    textAlign: 'center',
    padding: '5px',
  },
  wrap: {
    border: '1px solid darkcyan',
    display: 'flex',
    width: 1000,
    margin: '30px auto',
    justifyContent: "space-around"
  }
}));

function App() {
  const classes = useStyles();
  const [listUsers, setListUsers] = useState([])
  const userInfo = {
    email: '',
    firstName: '',
    lastName: '',
    avatarSrc: ''
  }

  const fetchData = async () => {
    const result = await axios.get('http://localhost:8888/cats/');
    console.log(result);
    setListUsers(result.data);
  };
  useEffect(() => {
    fetchData();

  }, [])
  return (
    <div>
      <div className={classes.header}>
        <h1 >
          Thao Bear</h1>
        <p> ... </p>

      </div>
      <div className={classes.wrap}>
        <CreateUserForm setListUsers={setListUsers} userInfo={userInfo} />
        <List id="user-list">
          {listUsers.length > 0 && listUsers.map(item => (
            <User setListUsers={setListUsers} key={item.id} user={item} />
          ))}
        </List>
      </div>
    </div>

  );
}

export default App;
