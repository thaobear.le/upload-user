import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import ListItemAvatar from "@material-ui/core/ListItemAvatar";
import Avatar from "@material-ui/core/Avatar";
import Typography from "@material-ui/core/Typography";
import DeleteIcon from "@material-ui/icons/Delete";
import EditDialog from "./EditDialog";
import { IconButton } from "@material-ui/core";
import axios from 'axios';
const useStyles = makeStyles((theme) => ({
    root: {
        width: "100%",
        maxWidth: "36ch",
        backgroundColor: theme.palette.background.paper,
    },
    inline: {
        display: "inline",
    },
    wrap: {
        display: 'flex'
    },
    container: {
        display: 'flex',
        flex: 1
    }
}));
const User = ({
    user: { id, email, firstName, lastName, avatar },
    setListUsers,
}) => {
    const userInfo = {
        id: id,
        email: email,
        firstName: firstName,
        lastName: lastName,
        avatar: avatar,
    };
    const classes = useStyles();
    async function handleDelete(id) {
        try { await axios.put('https://reqres.in/api/users', id); }
        catch (e) { console.log(e); }
        setListUsers((state) => state.filter((user) => user.id !== id));
    };


    return (
        <div className="card">
            <ListItem className="container">
                <div className={classes.container} >
                    <div style={{ display: 'flex', 'flex': 2 }} >

                        <ListItemAvatar>
                            <Avatar alt="Remy Sharp" src={avatar} />
                        </ListItemAvatar>
                        <ListItemText
                            primary={firstName + " " + lastName}
                            secondary={
                                <React.Fragment>
                                    <Typography
                                        component="span"
                                        variant="body2"
                                        className={classes.inline}
                                        color="textPrimary"
                                    >
                                        {email}
                                    </Typography>
                                </React.Fragment>
                            }
                        />
                    </div>
                    <div className={classes.wrap} >
                        <IconButton onClick={() => handleDelete(id)}>
                            {" "}
                            <DeleteIcon />
                        </IconButton>
                        <EditDialog userInfo={userInfo} setListUsers={setListUsers} />
                    </div>
                </div>
            </ListItem>
        </div>
    );
};

User.propTypes = {};

export default User;
