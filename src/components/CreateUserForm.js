import React, { useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import IconButton from "@material-ui/core/IconButton";
import PhotoCamera from "@material-ui/icons/PhotoCamera";
import axios from "axios";
const useStyles = makeStyles((theme) => ({

    root: {
        TextField: { color: "red" },
        padding: "10px",
        display: "inline-grid",
        "& > *": {
            margin: theme.spacing(1),
        },
        label: {
            // display: "none",
            border: "2px solid black",
        },
    },
    TextField: {},
}));

export default function CreateUserForm({ setListUsers, userInfo }) {
    const classes = useStyles();
    const [formValue, setFormValue] = useState({
        email: "",
        firstName: "",
        lastName: "",
        avatar: "",
    });

    const handleChange = (e) => {
        setFormValue({ ...formValue, [e.target.name]: e.target.value });
    };
    async function handleSubmit(e) {
        e.preventDefault();
        try {
            await axios.post("https://reqres.in/api/users", formValue);
            setListUsers((prevState) => [formValue, ...prevState]);
        } catch (e) {
            console.log(e);
        }
        setFormValue({
            email: "",
            firstName: "",
            lastName: "",
            avatar: ""
        })
    }
    const handleImageChange = (e) => {
        const image = e.target.files[0];
        const formData = new FormData();
        formData.append("image", image, image.name);

        setFormValue((state) => ({
            ...state,
            avatar: URL.createObjectURL(e.target.files[0]),
        }));
    };
    const handleEditPicture = () => {
        const fileInput = document.getElementById("imageInput");
        fileInput.click();
    };
    return (

        <div>
            <form
                id="left-form"
                onSubmit={handleSubmit}
                className={classes.root}
                autoComplete="on"
            >
                <TextField
                    onChange={handleChange}
                    variant="outlined"
                    name="email"
                    value={formValue.email}
                    id="email"
                    label="Email"
                    required
                />
                <TextField
                    onChange={handleChange}
                    variant="outlined"
                    name="firstName"
                    value={formValue.firstName}
                    id="firstName"
                    label="First name"
                    required
                />
                <TextField
                    onChange={handleChange}
                    variant="outlined"
                    name="lastName"
                    value={formValue.lastName}
                    id="lastName"
                    label="Last name"
                    required
                />
                <input
                    type="file"
                    id="imageInput"
                    hidden="hidden"
                    onChange={handleImageChange}
                    required
                />

                <img style={{ width: '100px' }} src={formValue.avatar} alt="" />
                <label onClick={handleEditPicture} htmlFor="icon-button-file">
                    <IconButton
                        color="primary"
                        aria-label="upload picture"
                        component="span"
                    >
                        <PhotoCamera />
                    </IconButton>
                </label>
                <div>
                    <Button type="submit" variant="outlined" color="primary">
                        Submit
          </Button>
                </div>
            </form>
        </div>
    );
}
