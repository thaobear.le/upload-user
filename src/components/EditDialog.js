import React from "react";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import EditIcon from "@material-ui/icons/Edit";
import IconButton from "@material-ui/core/IconButton";
import PhotoCamera from "@material-ui/icons/PhotoCamera";
import { makeStyles } from "@material-ui/core/styles";
import axios from 'axios';

const useStyles = makeStyles(
  (theme) => ({
    root: {
      "& > *": {
        margin: theme.spacing(1),
        width: "25ch",
      },
      input: {
        display: "none",
      },
    },
    TextField: {},
  }));
export default function EditDialog({ setListUsers, userInfo }) {
  const classes = useStyles();
  const [open, setOpen] = React.useState(false);
  const [formValue, setFormValue] = React.useState({
    email: userInfo.email,
    firstName: userInfo.firstName,
    lastName: userInfo.lastName,
    avatar: userInfo.avatar,
  });
  const handleChange = (e) => {
    setFormValue({ ...formValue, [e.target.name]: e.target.value });
  };
  const handleImageChange = (e) => {
    const image = e.target.files[0];
    const formData = new FormData();
    formData.append("image", image, image.name);
    setFormValue(state => ({ ...state, avatar: URL.createObjectURL(e.target.files[0]) }))
  };
  const handleEditPicture = () => {
    const fileInput = document.getElementById("imageInputEdit");
    fileInput.click();
  };
  const handleClickOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };
  async function handleUpdate(id) {
    setOpen(false);
    try { await axios.put('https://reqres.in/api/users', id, formValue); }
    catch (e) { console.log(e); }
    setListUsers((state) => [
      formValue,
      ...state.filter((user) => user.id !== id),
    ]);
  };

  return (
    <div>

      <IconButton onClick={handleClickOpen}>
        {" "}
        <EditIcon />
      </IconButton>
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="form-dialog-title"
      >
        <DialogTitle id="form-dialog-title">Edit User</DialogTitle>
        <DialogContent>
          <form className={classes.root} noValidate autoComplete="on">
            <div>
              <TextField
                onChange={handleChange}
                variant="outlined"
                name="email"
                value={formValue.email}
                id="email"
                label="Email"
              />
            </div>
            <TextField
              onChange={handleChange}
              name="firstName"
              value={formValue.firstName}
              id="firstName"
              label="First name"
            />
            <div>
              <TextField
                onChange={handleChange}
                variant="filled"
                name="lastName"
                value={formValue.lastName}
                id="lastName"
                label="Last name"
              />
            </div>
            <input
              type="file"
              id="imageInputEdit"
              hidden="hidden"
              onChange={handleImageChange}
            />
            <img src={formValue.avatar} alt="" />
            <label onClick={handleEditPicture} htmlFor="icon-button-file">
              <IconButton
                color="primary"
                aria-label="upload picture"
                component="span"
              >
                <PhotoCamera />
              </IconButton>
            </label>
          </form>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            Cancel
          </Button>
          <Button onClick={() => handleUpdate(userInfo.id)} color="primary">
            Update
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}

